import React from 'react';
import {Image, Text, View, StyleSheet, Color} from 'react-native';

const ImagePrin =(props) =>{
    
    const {foto}=props;
    console.log(foto);
    return(
        <View style={styles.stylesView}>
            <Image style={styles.stylesImage}
            source={{
                uri:foto
            }}/>
        </View>
    );
};
const styles=StyleSheet.create({
    stylesView:{
        paddingLeft:4,
        borderTopWidth:2,
        borderColor:"#ccc"
    },
    stylesImage:{
        resizeMode:"stretch",
        height:500,
        width:404,
       // borderRadius:400/2,
    }
});
export default ImagePrin;
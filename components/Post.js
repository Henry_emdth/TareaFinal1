import React from 'react';
import {Image, Text, View, StyleSheet, Color} from 'react-native';
const Post =(props) =>{
    const {nombre,image}=props;
    //console.log(image);
    return(
        <View style={styles.stylesView}>
            <Image style={styles.stylesImage}
            source={{
                uri:image
            }}/>
            <Text style={styles.stylesName}>{nombre} - </Text>
            <Text style={styles.stylesFollow}>Follow</Text>
            <Text style={styles.stylesMore}>...</Text>
        </View>
    );
}
const styles= StyleSheet.create({
    stylesView:{
        flexDirection:"row",
        paddingLeft:8,
        paddingTop:6,
        paddingBottom:6,
        borderTopWidth:2,
        borderColor:"#ccc",
    },
    stylesImage:{
        resizeMode:"stretch",
        height:40,
        width:40,
        borderRadius:400/2,
    },
    stylesName:{
        fontSize:15,
        fontWeight:"bold",
        paddingLeft:10,
        paddingTop:10
    },
    stylesFollow:{
        fontSize:15,
        fontWeight:"bold",
        color:"#3fdecd",
        paddingTop:10
    },
    stylesMore:{
        fontSize:20,
        fontWeight:"bold",
        paddingLeft:170,
        paddingTop:2
    }
});
export default Post;
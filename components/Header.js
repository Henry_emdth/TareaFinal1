import React from 'react';
import {Image, Text, View, StyleSheet, Color} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const Header =() =>{
    return(
        <View style={styles.stylesView}>
            <Icon style={styles.icono} 
                name={'camera'}
                size={28} 
                color={'black'}/>
            <Image style={styles.stylesImage}
                source={require('../src/logo.png')}/>
            <Icon style={styles.icono} 
                name={'send-o'}
                size={27} 
                color={'black'}/>
        </View>
    );
}
const styles= StyleSheet.create({
    stylesView:{
        height:55,
        flexDirection:"row",
        justifyContent:"space-between",
        paddingHorizontal:8,
        paddingVertical:6,        
        borderTopWidth:2,
        borderColor:"#ccc",
    },
    stylesImage:{
        resizeMode:"stretch",
        height:40,
        width:115,
        borderRadius:400/2,
    },
    stylesName:{
        fontSize:15,
        fontWeight:"bold",
        paddingLeft:10,
        paddingTop:10
    },
    stylesFollow:{
        fontSize:15,
        fontWeight:"bold",
        color:"#3fdecd",
        paddingTop:10
    },
    stylesMore:{
        fontSize:20,
        fontWeight:"bold",
        paddingLeft:170,
        paddingTop:2
    }
});
export default Header;
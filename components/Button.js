import React,{useState} from 'react';
import { View,StyleSheet,Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


const Button=(props)=>{
  const cantidad=0;
  const [estadoHeart, setEstadoHeart] = useState(false);
  const [estadoComment, setEstadoComment] = useState(false);
  const [estadoSend, setEstadoSend] = useState(false);
  const [estadoSave, setEstadoSave] = useState(false);

  const [like,setLike]=useState(cantidad);

  const updateHeart = () => {
    setEstadoHeart(!estadoHeart);
    if(estadoHeart){
      setLike(like-1);
    }else{
      setLike(like+1);
    }
  };
  const updateComment = () => {
    setEstadoComment(!estadoComment);
  };
  const updateSend = () => {
    setEstadoSend(!estadoSend);
  };
  const updateSave = () => {
    setEstadoSave(!estadoSave);
  };
    return(
    <View>
      <View style={styles.barra}>
        <View style={styles.icon}>
        {/* <Icon style={styles.icono} name='camera' size={32}/> */}
        <TouchableOpacity onPress={() => updateHeart()}>
          <Icon style={styles.icono} 
                name={estadoHeart ? 'heart' : 'heart-o'}
                size={32} 
                color={estadoHeart ? '#CB3716' : 'black'}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => updateComment()}>
          <Icon style={styles.icono} 
                name={estadoComment ? 'comment' : 'comment-o'} 
                size={32}
                color={estadoComment ? '#CB3716' : 'black'}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => updateSend()}>
          <Icon style={styles.icono} 
                name={estadoSend ? 'send' : 'send-o'} 
                size={32}
                color={estadoSend ? '#CB3716' : 'black'}/>
        </TouchableOpacity>                 
        </View>
        <View style={styles.save}>  
          <TouchableOpacity onPress={() => updateSave()}>
            <Icon style={styles.icono} 
                  name={estadoSave ? 'bookmark' : 'bookmark-o'} 
                  size={32}
                  color={estadoSave ? '#CB3716' : 'black'}/>
          </TouchableOpacity>            
        </View>       
      </View>
      <View style={styles.barra}>
          <View style={styles.icon}>
            <Icon style={styles.icono} name='heart' size={15}/>
              <Text>{like} Likes</Text>
          </View>
      </View>
        <Text style={styles.contenido}> {props.description}Lorem asd sa qwe rd</Text>
      <Text style={styles.coment}>ver los comentarios</Text>
    </View>

    );
}

const styles=StyleSheet.create({
    contenido:{
        marginHorizontal:20,
      },
      coment:{
        color:'gray',
        marginHorizontal:30,
        marginBottom:20,
        borderColor:'gray',
        //borderBottomWidth:1,
      },
      icono:{
        marginHorizontal:7,
      },
      save:{
       flexDirection:'row-reverse',
        //borderWidth:1,
        margin:10,
        flex:1,
        alignItems:"flex-end"
      },
      icon:{   
        flexDirection:'row',    
        //borderWidth:1,
        margin:10,  
        flex:2, 
        alignItems:"flex-start"
      },
      barra:{   
        flexDirection:'row',    
        //borderWidth:1,       
      },
});

export default Button;
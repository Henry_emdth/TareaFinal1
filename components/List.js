import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList
} from 'react-native';
import Button from './Button';
import Post from './Post';
import ImagePrin from './Image';

const List= (items) => {  
   console.log('-------');
   
   const {foto,image,nombre}=items.item;
//   console.log(foto);
  return (
   <View >
       <Post image={image} nombre={nombre}/>
        <ImagePrin foto={foto} />
        <Button/>        
   </View>
        
      
   
  );
};

const styles = StyleSheet.create({
    view:{
        flex:1
      }
});

export default List;

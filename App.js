import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView, 
  FlatList,
  View
} from 'react-native';

import Header from './components/Header';
import List from './components/List';

const items=[
  {id:1,
    foto:'https://www.antevenio.com/wp-content/uploads/2019/11/Series-de-IGTV.jpg',
    nombre:'Eliar Jhons - ',
    follow:'Follow',
    more:'...',
    image:'https://todoimagenesde.com/wp-content/uploads/2018/12/Leones2.jpg',
  },
  {id:2,
    foto:'https://images.clarin.com/2020/05/06/albert-einstein-en-1951-efe___sLtGuamGf_340x340__1.jpg',
    nombre:'Albert St - ',
    follow:'Follow',
    more:'...',
    image:'https://elcorso.es/wp-content/uploads/2011/09/fotos-albert-einstein-riendo12.jpg',
  },
  {id:3,
    foto:'https://i.blogs.es/030cf7/calabaza/450_1000.jpg',
    nombre:'Hollowen Next - ',
    follow:'Follow',
    more:'...',
    image:'https://cdn.flixbus.de/d7files/content-image/kuriosa-halloween.jpg',
  },
  {id:4,
    foto:'https://www.dzoom.org.es/wp-content/uploads/2011/08/insp-cuadradas-5.jpg',
    nombre:'MarasR - ',
    follow:'Follow',
    more:'...',
    image:'https://www.blogdelfotografo.com/wp-content/uploads/2019/08/retrato-mujer-pelo-711x1024.jpg',
  },
 
];

const App= () => {
  return (
    <View style={styles.view}>     
        <Header/>  
        
       <View>
       <FlatList
              renderItem={List}
              data={items}
              keyExtractor={(item) => String(item.id)}
              initialNumToRender={10}
              
          />  
         </View>   
         
           
         
            
    </View>
  );
};
const styles=StyleSheet.create({
  view:{
    flex:1
  }
});
export default App;
